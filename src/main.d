import std.stdio;
import std.path;
import std.process;
import gio.Resource;
import gtk.Main;

import app;

void main(string[] args) {
	
	//auto resource = Resource.load("gtkdappcreator-resources.gresource");
    //Resource.register(resource);
	
	//auto myVar = environment.get("MYVAR");
	environment["GSETTINGS_SCHEMA_DIR"] = "D:/projects/vala_projects/dist-vala/valatube/share/glib-2.0";
	
    Main.init(args);
    auto app = new GtkDApp();
    app.run(args);
}
