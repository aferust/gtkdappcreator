module app;

import std.stdio;
import std.experimental.logger;

import gio.Application : GApplication = Application;
import gtk.Application;
import gtk.Builder;

import appwindow;

class GtkDApp : Application
{

public:

    this()
    {   
        ApplicationFlags flags = ApplicationFlags.FLAGS_NONE;
        super("org.gnome.gtkdappcreator", flags);
        this.addOnActivate(&onAppActivate);
        this.window = null;
    }

private:

    AppWindow window;

    void onAppActivate(GApplication app)
    {
        trace("Activate App Signal");
        if (!app.getIsRemote())
        {
            this.window = new AppWindow(this);
        }

        this.window.present();
    }
}
