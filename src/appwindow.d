module appwindow;

import std.experimental.logger;

import standardpaths;

import gtk.ApplicationWindow;
import gtk.Builder;
import gtk.HeaderBar;
import gtk.Entry;
import gtk.TextView;
import gtk.TextIter;
import gtk.Label;
import gtk.Button;
import gtk.Box;
import gtk.FileChooserDialog;
import gtk.Dialog;
import glib.Timeout;
import std.string;
import std.file;
import std.path;
import std.process;
import std.stdio;
import app;
import creator;

import windowglade;

class AppWindow: ApplicationWindow {
private:
	Builder builder;
	Entry entryName;
	Entry entryGtkDVer;
	TextView textView1;
	Button fbut;
	Button cbut;
	Label labelOut;
	FileChooserDialog s_dialog;
	string rootpath;
public:
    this(gtk.Application.Application application){
        
        builder = new Builder();
        //if(!builder.addFromString(cast(string)std.file.read("window.ui"))){
        //if(!builder.addFromResource("/org/gnome/Gtkdappcreator/window.ui")){
        if(!builder.addFromString(WindowGlade.getGladeStr())){
            critical("Window resource cannot be found");
        }
        
        auto s = builder.getObject("window");
		auto win = cast(GtkApplicationWindow*)s.getObjectGStruct();
		super(win);
		this.setApplication(application); // this is crucial!!!
        
        this.setBorderWidth(10);
        //this.setDefaultSize(640, 480);
        
        auto folderFlag = FolderFlag.verify;
        rootpath = writablePath(StandardPath.desktop, folderFlag);
        
        labelOut = cast(Label)builder.getObject("labelOut");
        labelOut.setLabel(rootpath);
        
        entryName = cast(Entry)builder.getObject("entryName");
        entryName.setText("helloworld");
        entryGtkDVer = cast(Entry)builder.getObject("entryGtkDVer");
        entryGtkDVer.setText("3.8.4");
        
        textView1 = cast(TextView)builder.getObject("textView1");
        
        fbut = cast(Button)builder.getObject("fbut");
        cbut = cast(Button)builder.getObject("cbut");
        
        entryName.addOnChanged ((editable){
			labelOut.setLabel(rootpath ~ dirSeparator ~ strip(entryName.getText()));
		});
        
        fbut.addOnClicked( delegate void(Button aux){
			string[] a;
			a ~= "_Cancel";
			a ~= "_Select";
			ResponseType[] r;
			r ~= ResponseType.CANCEL;
			r ~= ResponseType.ACCEPT;
			
			s_dialog = new FileChooserDialog ("Set a root folder for the project",
															 this,
															 FileChooserAction.SELECT_FOLDER,
															 a,
															 r);
			s_dialog.setModal(true);
			s_dialog.addOnResponse(&set_as_response_cb);
			s_dialog.run();
		});
		
		cbut.addOnClicked( delegate void(Button aux){
			auto ctor = new Creator(rootpath, strip(entryName.getText()));
			if(!ctor.make_root()){
				textView1.appendText("Folder exists already!\n");
			}else{
				textView1.appendText("Root folder was created!\n");
				textView1.appendText(ctor.make_src_folder() ~ "\n");
			}
			
		});
    }
    
    void set_as_response_cb(int response_id, Dialog dialog){
		s_dialog = cast(FileChooserDialog)dialog;
		s_dialog.setTransientFor(this);
		switch (response_id) {
			case ResponseType.ACCEPT:
				rootpath = s_dialog.getFilename();
				writeln(rootpath);
				labelOut.setText(rootpath ~ dirSeparator ~ entryName.getText());
				break;
			case ResponseType.CANCEL:
				break;
			case ResponseType.CLOSE:
				break;
			default:
				break;
		}
		
		s_dialog.destroy();			
	}
}

