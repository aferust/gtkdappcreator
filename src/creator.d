module creator;

import std.stdio: toFile;
import std.string;
import std.conv;
import std.file : mkdir, isDir, exists;
import std.path;
import std.format;
import std.stdio;

import templates;

extern (C) int is_win();

class Creator {
	private:
		string path;
		string name;
		string Name;
		string fullpath;
		
	public:	
		this(string path, string name) {
			this.path = path;
			this.name = name;
			
			string first = to!string(toUpper(name[0]));
			string rest = name[1..name.length];
			this.Name = first ~ rest;
			
			fullpath = path ~ dirSeparator ~ name;
		}
		
		bool make_root(){
			if(!exists(fullpath)){
				
				mkdir(fullpath);
				mkdir(fullpath ~ dirSeparator ~ "build");
				mkdir(fullpath ~ dirSeparator ~ "data");
				mkdir(fullpath ~ dirSeparator ~ "po");
				mkdir(fullpath ~ dirSeparator ~ "dub_packs");
				mkdir(fullpath ~ dirSeparator ~ "build-aux");
				mkdir(fullpath ~ dirSeparator ~ "build-aux" ~ dirSeparator ~ "meson");
				
				string appdata_xml_in = format(Templates.get_appdata_xml_in(), Name);
				appdata_xml_in.toFile(fullpath ~ dirSeparator ~ "data" ~ dirSeparator ~ "org.gnome." ~ Name ~ ".appdata.xml.in");
				
				string desktop_in = format(Templates.get_desktop_in(), name, name);
				desktop_in.toFile(fullpath ~ dirSeparator ~ "data" ~ dirSeparator ~ "org.gnome." ~ Name ~ ".desktop.in");
				
				string gschema_xml = format(Templates.get_gschema_xml(), name, Name, Name);
				gschema_xml.toFile(fullpath ~ dirSeparator ~ "data" ~ dirSeparator ~ "org.gnome." ~ Name ~ ".gschema.xml");
				
				string data_mesonbuild = format(Templates.get_data_mesonbuild(), Name, Name, Name, Name, Name);
				data_mesonbuild.toFile(fullpath ~ dirSeparator ~ "data" ~ dirSeparator ~ "meson.build");
				
				string potfiles = format(Templates.get_potfiles(), Name, Name, Name);
				potfiles.toFile(fullpath ~ dirSeparator ~ "po" ~ dirSeparator ~ "POTFILES");
				
				" ".toFile(fullpath ~ dirSeparator ~ "po" ~ dirSeparator ~ "LINGUAS");
				
				string po_meson = "i18n.gettext('" ~ name ~ "', preset: 'glib')";
				po_meson.toFile(fullpath ~ dirSeparator ~ "po" ~ dirSeparator ~ "meson.build");
				
				string dubpacks_readme = Templates.get_dubpacks_readme();
				dubpacks_readme.toFile(fullpath ~ dirSeparator ~ "dub_packs" ~ dirSeparator ~ "readme_build_gtkd.txt");
				
				string post_install = Templates.get_post_install();
				post_install.toFile(fullpath ~ dirSeparator ~ "build-aux" ~ dirSeparator ~ "meson" ~ dirSeparator ~ "postinstall.py");
				
				string root_meson = format(Templates.get_root_meson(), name);
				root_meson.toFile(fullpath ~ dirSeparator ~ "meson.build");
				
				string geany_file;
				
				if(is_win() == 1){
					string safepath = replace(fullpath, "\\", "\\\\");
					geany_file = format(Templates.get_geany_file(), name, safepath, name, name);
				}else{
					geany_file = format(Templates.get_geany_file(), name, fullpath, name, name);
				}
				geany_file.toFile(fullpath ~ dirSeparator ~ Name ~ ".geany");
				
				return true;
			}else{
				return false;
			}
		}
		
		string make_src_folder(){
			mkdir(fullpath ~ dirSeparator ~ "src");
			
			string main_template = Templates.get_main_template();
			main_template.toFile(fullpath ~ dirSeparator ~ "src" ~ dirSeparator ~ "main.d");
			
			string app_template = format(Templates.get_app_template(), name);
			app_template.toFile(fullpath ~ dirSeparator ~ "src" ~ dirSeparator ~ "app.d");
			
			string appwindow_template = format(Templates.get_appwindow_template(), Name);
			appwindow_template.toFile(fullpath ~ dirSeparator ~ "src" ~ dirSeparator ~ "appwindow.d");
			
			string src_gresource_file = format(Templates.get_src_gresource_file(), Name);
			src_gresource_file.toFile(fullpath ~ dirSeparator ~ "src" ~ dirSeparator ~ name ~ ".gresource.xml");
			
			string window_ui = Templates.get_window_ui();
			window_ui.toFile(fullpath ~ dirSeparator ~ "src" ~ dirSeparator ~ "window.ui");
			
			string src_meson = format(Templates.get_src_meson(),
				name, name, name, name, name, name, name, name, name, name, name);
			src_meson.toFile(fullpath ~ dirSeparator ~ "src" ~ dirSeparator ~ "meson.build");
			
			string src_res_builder = Templates.getResourceBuilderScript();
			src_res_builder.toFile(fullpath ~ dirSeparator ~ "src" ~ dirSeparator ~ "resource_builder.py");
			
			return "src folder was created!";
		}
}
