module templates;

class Templates{
    public static string get_main_template(){
	return "import std.stdio;
import std.path;
import std.process;
import gio.Resource;
import gtk.Main;

import app;

void main(string[] args) {
	
	//auto resource = Resource.load(\"gtkdappcreator-resources.gresource\");
    //Resource.register(resource);
	
    Main.init(args);
    auto app = new GtkDApp();
    app.run(args);
}  
";
	}
	
    public static string get_app_template(){
	// lower
	return "module app;

import std.stdio;
import std.experimental.logger;

import gio.Application : GApplication = Application;
import gtk.Application;
import gtk.Builder;

import appwindow;

class GtkDApp : Application
{

public:

    this()
    {   
        ApplicationFlags flags = ApplicationFlags.FLAGS_NONE;
        super(\"org.gnome.%s\", flags);
        this.addOnActivate(&onAppActivate);
        this.window = null;
    }

private:

    AppWindow window;

    void onAppActivate(GApplication app)
    {
        trace(\"Activate App Signal\");
        if (!app.getIsRemote())
        {
            this.window = new AppWindow(this);
        }

        this.window.present();
    }
}		
		
";
	}
    public static string get_appwindow_template(){
	// upper
	return "module appwindow;

import std.experimental.logger;

import gtk.ApplicationWindow;
import gtk.Builder;
import gtk.HeaderBar;
import gtk.Entry;
import gtk.Label;
import gtk.Button;
import gtk.Box;
import glib.Timeout;
import std.file;

import windowglade;
import app;

class AppWindow: ApplicationWindow {
private:
	Builder builder;
	Label label1;
	Button but1;
	Entry entry1;
	Box hbox1;
public:
    this(gtk.Application.Application application){
        
        builder = new Builder();
        //if(!builder.addFromString(cast(string)std.file.read(\"window.ui\"))){
        //if(!builder.addFromResource(\"/org/gnome/%s/window.ui\")){
        if(!builder.addFromString(WindowGlade.getGladeStr())){
            critical(\"Window resource cannot be found\");
        }
        
        auto s = builder.getObject(\"window\");
		auto win = cast(GtkApplicationWindow*)s.getObjectGStruct();
		super(win);
		this.setApplication(application); // this is crucial!!!
        
        this.setBorderWidth(10);
        this.setDefaultSize(640, 480);
        
        hbox1 = cast(Box)builder.getObject(\"hbox1\");
        hbox1.setSpacing(5);
        
        label1 = cast(Label)builder.getObject(\"label1\");
        label1.setLabel(\"Hello buddy!\");
        
        entry1 = cast(Entry)builder.getObject(\"entry1\");
        
        but1 = cast(Button)builder.getObject(\"but1\");
        
        but1.addOnClicked( delegate void(Button aux){
			entry1.setText(\"Good job!\"); 
			aux.setLabel(\"??????!\");
			new Timeout(2000, delegate bool(){
				but1.setLabel(\"Hmmm!\");
				return false;
			}, false);
			
		});
    }

}		
";
    }
	
    public static string get_src_gresource_file(){
	// upper upper
	return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<gresources>
  <gresource prefix=\"/org/gnome/%s\">
    <file>window.ui</file>
  </gresource>
</gresources>
";
    }
    
    public static string get_window_ui(){
	return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<!-- Generated with glade 3.20.3 -->
<interface>
  <requires lib=\"gtk+\" version=\"3.20\"/>
  <object class=\"GtkApplicationWindow\" id=\"window\">
    <property name=\"can_focus\">False</property>
    <child>
      <object class=\"GtkBox\" id=\"hbox1\">
        <property name=\"visible\">True</property>
        <property name=\"can_focus\">False</property>
        <property name=\"orientation\">vertical</property>
        <child>
          <object class=\"GtkLabel\" id=\"label1\">
            <property name=\"visible\">True</property>
            <property name=\"can_focus\">False</property>
            <property name=\"label\" translatable=\"yes\">label</property>
          </object>
          <packing>
            <property name=\"expand\">False</property>
            <property name=\"fill\">True</property>
            <property name=\"position\">0</property>
          </packing>
        </child>
        <child>
          <object class=\"GtkEntry\" id=\"entry1\">
            <property name=\"visible\">True</property>
            <property name=\"can_focus\">True</property>
          </object>
          <packing>
            <property name=\"expand\">False</property>
            <property name=\"fill\">True</property>
            <property name=\"position\">1</property>
          </packing>
        </child>
        <child>
          <object class=\"GtkButton\" id=\"but1\">
            <property name=\"label\" translatable=\"yes\">button</property>
            <property name=\"visible\">True</property>
            <property name=\"can_focus\">True</property>
            <property name=\"receives_default\">True</property>
          </object>
          <packing>
            <property name=\"expand\">False</property>
            <property name=\"fill\">True</property>
            <property name=\"position\">2</property>
          </packing>
        </child>
      </object>
    </child>
    <child type=\"titlebar\">
      <object class=\"GtkHeaderBar\" id=\"headerBar\">
        <property name=\"visible\">True</property>
        <property name=\"can_focus\">False</property>
        <property name=\"title\">Hello GtkD</property>
        <property name=\"show_close_button\">True</property>
        <child>
          <object class=\"GtkImage\">
            <property name=\"visible\">True</property>
            <property name=\"can_focus\">False</property>
            <property name=\"stock\">gtk-home</property>
          </object>
        </child>
      </object>
    </child>
  </object>
</interface>		
";
    }
	
    public static string get_src_meson(){
	// 11 x lower
	return r"r = run_command('python', join_paths(meson.source_root(), 'src', 'resource_builder.py'))
message(r.stdout().strip())
%s_sources = [
  'main.d',
  'app.d',
  'windowglade.d',
  'appwindow.d',
]

dub_exe = find_program('dub', required : false)
if not dub_exe.found()
  error('MESON_SKIP_TEST: Dub not found')
endif

r = run_command('dub', 'add-path', join_paths(meson.source_root(), 'dub_packs', 'gtk-d-3.8.4'))
gtkd_dep = dependency('gtk-d:gtkd', version: '>=3.8.4', method: 'dub')

gtkd_inc = include_directories(['../dub_packs/gtk-d-3.8.4/gtk-d/generated/gtkd'])

%s_deps = [
	gtkd_dep,
]

gnome = import('gnome')
%s_resources = gnome.compile_resources('%s-resources',
  '%s.gresource.xml',
  c_name: '%s',
  gresource_bundle: true,
)

%s_sources += %s_resources

executable('%s', %s_sources,
  dependencies: %s_deps,
  link_args : ['-mscrtlib=', '-linker=lld-link'],
  install: true,
  include_directories : [gtkd_inc],
  #link_with : [foolib],
  #gui_app : true,
)
";
    }
    
    public static string get_root_meson(){
	//
	return r"project('%s', ['c', 'd', 'cpp'],
  version: '0.1.0',
  meson_version: '>= 0.40.0',
)

i18n = import('i18n')

if host_machine.system() != 'windows'
	subdir('data')
endif

subdir('src')
subdir('po')

meson.add_install_script('build-aux/meson/postinstall.py')
";
    }
    
    public static string get_geany_file(){
	return r"[editor]
line_wrapping=false
line_break_column=72
auto_continue_multiline=true

[file_prefs]
final_new_line=true
ensure_convert_new_lines=false
strip_trailing_spaces=false
replace_tabs=false

[indentation]
indent_width=4
indent_type=1
indent_hard_tab_width=8
detect_indent=false
detect_indent_width=false
indent_mode=2

[project]
name=%s
base_path=%s
description=
file_patterns=

[long line marker]
long_line_behaviour=1
long_line_column=72

[build-menu]
EX_00_LB=_Execute
EX_00_CM=%%p/build/src/%s
EX_00_WD=
filetypes=D;
EX_01_LB=_Debug
EX_01_CM=gdb %%p/build/src/%s
EX_01_WD=
DFT_00_LB=_Compile
DFT_00_CM=meson ..
DFT_00_WD=%%p/build/
DFT_01_LB=_Build
DFT_01_CM=ninja
DFT_01_WD=%%p/build/
DFT_02_LB=Clean
DFT_02_CM=ninja clean
DFT_02_WD=%%p/build/

[prjorg]
source_patterns=*.c;*.C;*.cpp;*.cxx;*.c++;*.cc;*.m;
header_patterns=*.h;*.H;*.hpp;*.hxx;*.h++;*.hh;
ignored_dirs_patterns=.*;CVS;
ignored_file_patterns=*.o;*.obj;*.a;*.lib;*.so;*.dll;*.lo;*.la;*.class;*.jar;*.pyc;*.mo;*.gmo;
generate_tag_prefs=0
external_dirs=

[files]

";
    }
    
    public static string get_post_install(){
	return r"#!/usr/bin/env python3

from os import environ, path
from subprocess import call

prefix = environ.get('MESON_INSTALL_PREFIX', '/usr/local')
datadir = path.join(prefix, 'share')
destdir = environ.get('DESTDIR', '')

# Package managers set this so we don't need to run
if not destdir:
    print('Updating icon cache...')
    call(['gtk-update-icon-cache', '-qtf', path.join(datadir, 'icons', 'hicolor')])

    print('Updating desktop database...')
    call(['update-desktop-database', '-q', path.join(datadir, 'applications')])

    print('Compiling GSettings schemas...')
    call(['glib-compile-schemas', path.join(datadir, 'glib-2.0', 'schemas')])
";
    }
    
    public static string get_dubpacks_readme(){
	return r"
to get and build gtkd run following commands in this folder:

dub fetch gtk-d --cache=local -v
cd gtk-d-3.8.4/gtk-d
dub

";
    }
    
    public static string get_appdata_xml_in(){
	return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<component type=\"desktop\">
	<id>org.gnome.%s.desktop</id>
	<metadata_license>CC0-1.0</metadata_license>
	<project_license>LGPL-3.0-or-later</project_license>
	<description>
	</description>
</component>	
";
    }
    public static string get_desktop_in(){
	return "[Desktop Entry]
Name=%s
Exec=%s
Terminal=false
Type=Application
Categories=GTK;
StartupNotify=true
";
    }
    
    public static string get_gschema_xml(){
	// l u u
	return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<schemalist gettext-domain=\"%s\">
	<schema id=\"org.gnome.%s\" path=\"/org/gnome/%s/\">
	</schema>
</schemalist>
";
    }
    
    public static string get_data_mesonbuild(){
	// u u u u u
	return "desktop_file = i18n.merge_file(
  input: 'org.gnome.%s.desktop.in',
  output: 'org.gnome.%s.desktop',
  type: 'desktop',
  po_dir: '../po',
  install: true,
  install_dir: join_paths(get_option('datadir'), 'applications')
)

desktop_utils = find_program('desktop-file-validate', required: false)
if desktop_utils.found()
  test('Validate desktop file', desktop_utils,
    args: [desktop_file]
  )
endif

appstream_file = i18n.merge_file(
  input: 'org.gnome.%s.appdata.xml.in',
  output: 'org.gnome.%s.appdata.xml',
  po_dir: '../po',
  install: true,
  install_dir: join_paths(get_option('datadir'), 'appdata')
)

appstream_util = find_program('appstream-util', required: false)
if appstream_util.found()
  test('Validate appstream file', appstream_util,
    args: ['validate', appstream_file]
  )
endif

install_data('org.gnome.%s.gschema.xml',
  install_dir: join_paths(get_option('datadir'), 'glib-2.0/schemas')
)

compile_schemas = find_program('glib-compile-schemas', required: false)
if compile_schemas.found()
  test('Validate schema file', compile_schemas,
    args: ['--strict', '--dry-run', meson.current_source_dir()]
  )
endif
";
    }
    
    public static string get_potfiles(){
	return "data/org.gnome.%s.desktop.in
data/org.gnome.%s.appdata.xml.in
data/org.gnome.%s.gschema.xml
src/window.ui
src/main.d
src/appwindow.d
src/app.d	
";
    }
    
    public static string getResourceBuilderScript(){
		return `import os

extension = ".ui";

list_of_files = [file for file in os.listdir("./") if file.lower().endswith(extension)];

template = """module {module_name};

class {class_name}{{
    public static string getGladeStr(){{
        ubyte[] ub = {byte_string};
        return cast(string)ub;
    }}
}}
/*
use builder like:
if(!builder.addFromString(WindowGlade.getGladeStr())){{
    critical("Window resource cannot be found");
}}
*/
"""

for mfile in list_of_files:
    filename = os.path.splitext(mfile)[0];
    classname = filename[0].upper() + filename[1:] + "Glade"
    
    data = ""
    with open(mfile) as file:  
        data = file.read()
    
    arraystr = []
    arraystr.append("[\n")
    i = 0
    for ch in data:
        arraystr.append(hex(ord(ch)))
        arraystr.append(",")
        if i % 12 == 0 and i != 0: arraystr.append("\n")
        i = i + 1
    del arraystr[-1]
    arraystr.append("]")
    mybytesstr = ''.join(arraystr)
    
    filecontent = template.format(module_name=filename+"glade", class_name=classname, byte_string=mybytesstr)
    
    file = open(filename + "glade.d","w") 
    file.write(filecontent)
    file.close()`;
	}
}
