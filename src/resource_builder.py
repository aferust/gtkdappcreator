import os

extension = ".ui";

list_of_files = [file for file in os.listdir("./") if file.lower().endswith(extension)];

template = """module {module_name};

class {class_name}{{
    public static string getGladeStr(){{
        ubyte[] ub = {byte_string};
        return cast(string)ub;
    }}
}}
/*
use builder like:
if(!builder.addFromString(WindowGlade.getGladeStr())){{
    critical("Window resource cannot be found");
}}
*/
"""

for mfile in list_of_files:
    filename = os.path.splitext(mfile)[0];
    classname = filename[0].upper() + filename[1:] + "Glade"
    
    data = ""
    with open(mfile) as file:  
        data = file.read()
    
    arraystr = []
    arraystr.append("[\n")
    i = 0
    for ch in data:
        arraystr.append(hex(ord(ch)))
        arraystr.append(",")
        if i % 12 == 0 and i != 0: arraystr.append("\n")
        i = i + 1
    del arraystr[-1]
    arraystr.append("]")
    mybytesstr = ''.join(arraystr)
    
    filecontent = template.format(module_name=filename+"glade", class_name=classname, byte_string=mybytesstr)
    
    file = open(filename + "glade.d","w") 
    file.write(filecontent)
    file.close()
    
